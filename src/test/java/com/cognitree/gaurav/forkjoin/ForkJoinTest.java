package com.cognitree.gaurav.forkjoin;

import java.util.concurrent.ExecutionException;

public class ForkJoinTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ForkJoin fj = new ForkJoin();
        fj.makeThreadPools();
        System.out.println("Word Count of the thread class");
    }
}
//185ms - 10 lakh words
//350 ms, 350 ms, 394 ms, 348 ms- fork join. 60 lakh words