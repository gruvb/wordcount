package com.cognitree.gaurav.forkjoin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class ThreadCountOneFile extends RecursiveTask {
    public List<Map<String, Integer>> getThreadWordCounts() {
        return threadWordCounts;
    }

    static List<Map<String, Integer>> threadWordCounts = new ArrayList<>();
    List<String[]> totalLinesInFile = new ArrayList<>();
    ConcurrentHashMap<String,Integer> wordCount;
    int start;
    int end;


    public ThreadCountOneFile(ConcurrentHashMap<String,Integer> wordCount, List<String[]> totalLinesInFile, int start, int end) {
        this.totalLinesInFile = totalLinesInFile;
        this.wordCount = new ConcurrentHashMap<String,Integer>();
        this.start = start;
        this.end = end;
    }

    //parallelisgm works at scale.
    public Map<String, Integer> getWordCount() {
        return this.wordCount;
    }


    @Override
    protected List<Map<String, Integer>> compute() {
        if (end - start < 100) {
            computeDirectly();
        } else {
            int middle = (end + start) / 2;
            ThreadCountOneFile subTask1 = new ThreadCountOneFile(wordCount, totalLinesInFile, start, middle);
            ThreadCountOneFile subTask2 = new ThreadCountOneFile(wordCount, totalLinesInFile, middle, end);

            ForkJoinTask.invokeAll(subTask1, subTask2);
        }
        return null;
    }

    private ConcurrentHashMap<String,Integer> computeDirectly() {
        try {
            // Display map
            for (int line = start; line < end; line++) {
                for (int word = 0; word < totalLinesInFile.get(line).length; word++) {
                    String currentWord = totalLinesInFile.get(line)[word];
                    if(wordCount.containsKey(currentWord)) {
                        wordCount.put(currentWord, wordCount.get(currentWord) + 1);
                    } else {
                        wordCount.put(currentWord, 1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("got in the compute method.");
//        System.out.println(wordCount);
        threadWordCounts.add(wordCount);
        return wordCount;
    }

}
