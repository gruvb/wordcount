package com.cognitree.gaurav.forkjoin;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class ForkJoin {
    ConcurrentHashMap<String, Integer> wordCount = new ConcurrentHashMap<String, Integer>();

    Map<String, Integer> totalWordCount = new HashMap<String, Integer>();

    List<String[]> totalLinesInFile = new ArrayList<>();


    public void readFile() {
        List<Map<String, Integer>> threadWordCounts = new ArrayList<>();
        Map<String, Integer> totalWordCount = new HashMap<String, Integer>();

        String fileLocation = "/Users/gc/IdeaProjects/WordCount/src/main/java/sampleFICT.txt";
        try (BufferedReader br = new BufferedReader(
                new FileReader(fileLocation))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split("\\r?\\n");
                List<String> wordList = new ArrayList<>();
                String[] words = lines[0].split(" ");
                for (int i = 0; i < words.length; i++) {
                    wordList.add(words[i]);
                }
                totalLinesInFile.add(words);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Total Lines in File: " + totalLinesInFile.size());
    }

    public void makeThreadPools() throws ExecutionException, InterruptedException {
        readFile();

        long startTime = System.nanoTime();

        ThreadCountOneFile mainTask = new ThreadCountOneFile(wordCount, totalLinesInFile, 0, totalLinesInFile.size());
        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(mainTask);
        pool.shutdown();

//        System.out.println("this class's word count" + wordCount);
//        System.out.println("main's words" + mainTask.threadWordCounts);
//        System.out.println(mainTask.threadWordCounts);


        long elapsedTime = System.nanoTime() - startTime;
        System.out.println("Total execution time in millis: "
                + elapsedTime/1000000);

        for (Map<String, Integer> wcount : mainTask.threadWordCounts) {
            for (Map.Entry<String, Integer> entry : wcount.entrySet()) {
                String iword = entry.getKey();
                Integer icount = entry.getValue();
                Integer count = totalWordCount.get(iword);
                totalWordCount.put(iword, count == null ? icount : count + icount);
            }
        }

        System.out.println(totalWordCount);

//        System.out.println(mainTask.getThreadWordCounts());

        int countValues = 0;
        for(Map.Entry<String, Integer> map: totalWordCount.entrySet()) {
            countValues += map.getValue();
        }
        System.out.println("total's count" + countValues);//1000000

    }
}
