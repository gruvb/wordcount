package com.cognitree.gaurav.threads;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Threads {

    List<String[]> totalLinesInFile = new ArrayList<>();
    Map<String, Integer> totalWordsCount = new HashMap<String, Integer>();
    List<Map<String, Integer>> threadWordCounts = new ArrayList<>();
    Map<String, Integer> totalWordCount = new HashMap<String, Integer>();

    public void readFile(){

        String fileLocation = "/Users/gc/IdeaProjects/WordCount/src/main/java/sampleFICT.txt";
        try (BufferedReader br = new BufferedReader(
                new FileReader(fileLocation))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split("\\r?\\n");
                List<String> wordList = new ArrayList<>();
                String[] words = lines[0].split(" ");
                for (int i = 0; i < words.length; i++) {
                    wordList.add(words[i]);
                }
                totalLinesInFile.add(words);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Total Lines in File: " + totalLinesInFile.size());
    }

    public void evaluateThreads() throws InterruptedException {
        readFile();

        long startTime = System.nanoTime();


        //get threads to operate over
        ThreadCountOneFile threadCountOneFile1 = new ThreadCountOneFile(totalLinesInFile, 0);
        ThreadCountOneFile threadCountOneFile2 = new ThreadCountOneFile(totalLinesInFile, 2000);
        ThreadCountOneFile threadCountOneFile3 = new ThreadCountOneFile(totalLinesInFile, 4000);


        threadCountOneFile1.start();
        threadCountOneFile2.start();
        threadCountOneFile3.start();

        //only scheduling. can't force parallel sceduling.
        threadCountOneFile1.join();//main threads stops[pauses unitl this is done. t2 and t3 keeps running.
        threadCountOneFile2.join();
        threadCountOneFile3.join();

        threadWordCounts.add(threadCountOneFile1.getWordCount());
        threadWordCounts.add(threadCountOneFile2.getWordCount());
        threadWordCounts.add(threadCountOneFile3.getWordCount());




        long elapsedTime = System.nanoTime() - startTime;
        System.out.println("Total execution time in millis: "
                + elapsedTime/1000000);


        mergeHashMaps();
        countWords();
    }

    private void mergeHashMaps() {
        for ( Map<String, Integer> wcount: threadWordCounts ) {
            for (Map.Entry<String, Integer> entry : wcount.entrySet())
            {
                String iword = entry.getKey();
                Integer icount = entry.getValue();
                Integer count = totalWordCount.get(iword);
                totalWordCount.put(iword, count == null ? icount : count + icount);
            }
        }
    }

    private void countWords() {
        int countValues = 0;
        for(Map.Entry<String, Integer> map: totalWordCount.entrySet()) {
            countValues += map.getValue();
        }
        System.out.println(totalWordCount.size());
        System.out.println("total word count" + countValues);//1000000
    }

}
