package com.cognitree.gaurav.threads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ThreadCountOneFile extends Thread {
    private String _filename;
    private Map<String, Integer> wordCount;
    List<String[]> totalLinesInFile = new ArrayList<>();
    int lineNumber;

    public ThreadCountOneFile(List<String[]> totalLinesInFile, int lineNumber) {
        this.totalLinesInFile = totalLinesInFile;
        this.lineNumber = lineNumber;
        this.wordCount = new HashMap<String, Integer>();
    }

    public Map<String, Integer> getWordCount() {
        return this.wordCount;
    }

    @Override
    public void run() {
        try {
            for (int line = lineNumber; line < lineNumber+2000; line++) {
                for (int word = 0; word < totalLinesInFile.get(line).length; word++) {
                    String currentWord = totalLinesInFile.get(line)[word];
                    if(wordCount.containsKey(currentWord)) {
                        wordCount.put(currentWord, wordCount.get(currentWord) + 1);
                    } else {
                        wordCount.put(currentWord, 1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
