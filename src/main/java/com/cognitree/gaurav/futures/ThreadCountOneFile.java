package com.cognitree.gaurav.futures;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

class ThreadCountOneFile implements Callable {
    List<String[]> totalLinesInFile = new ArrayList<>();
    int lineNumber;
    ConcurrentHashMap<String,Integer> wordCount;

    public ThreadCountOneFile(ConcurrentHashMap<String,Integer> wordCount, List<String[]> totalLinesInFile, int lineNumber) {
        this.totalLinesInFile = totalLinesInFile;
        this.lineNumber = lineNumber;
        this.wordCount = new ConcurrentHashMap<String,Integer>();
    }

    //parallelisgm works at scale.
    public Map<String, Integer> getWordCount() {
        return this.wordCount;
    }

    @Override
    public ConcurrentHashMap<String,Integer> call() {
        try {
            // Display map

            for (int line = lineNumber; line < lineNumber+2000; line++) {
                for (int word = 0; word < totalLinesInFile.get(line).length; word++) {
                    String currentWord = totalLinesInFile.get(line)[word];
                    if(wordCount.containsKey(currentWord)) {
                        wordCount.put(currentWord, wordCount.get(currentWord) + 1);
                    } else {
                        wordCount.put(currentWord, 1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        System.out.println("words" + wordCount);
//        System.out.println(lineNumber);
        return wordCount;
    }
}
