package com.cognitree.gaurav.sequential;

import org.omg.PortableInterceptor.INACTIVE;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sequential {

    List<String[]> totalLinesInFile = new ArrayList<>();
    Map<String, Integer> totalWordsCount = new HashMap<String, Integer>();

    public void readFile(){
        List<Map<String, Integer>> threadWordCounts = new ArrayList<>();
        Map<String, Integer> totalWordCount = new HashMap<String, Integer>();

        String fileLocation = "/Users/gc/IdeaProjects/WordCount/src/main/java/sampleFICT.txt";
        try (BufferedReader br = new BufferedReader(
                new FileReader(fileLocation))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split("\\r?\\n");
                List<String> wordList = new ArrayList<>();
                String[] words = lines[0].split(" ");
                for (int i = 0; i < words.length; i++) {
                    wordList.add(words[i]);
                }
                totalLinesInFile.add(words);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Total Lines in File: " + totalLinesInFile.size());
    }

    public void evaluate() {
        readFile();
        long startTime = System.nanoTime();


        for (int line = 0; line < totalLinesInFile.size(); line++) {
            for (int word = 0; word < totalLinesInFile.get(line).length; word++) {
                String currentWord = totalLinesInFile.get(line)[word];
                if(totalWordsCount.containsKey(currentWord)) {
                    totalWordsCount.put(currentWord, totalWordsCount.get(currentWord) + 1);
                } else {
                    totalWordsCount.put(currentWord, 1);
                }
            }
        }

        long elapsedTime = System.nanoTime() - startTime;


        System.out.println("Total execution time in Java in millis: "
                + elapsedTime/1000000);

        int count = 0;
        for(Map.Entry<String, Integer> map: totalWordsCount.entrySet()) {
            count += map.getValue();
        }
        System.out.println(count);//1000000

    }

}
