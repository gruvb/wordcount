package com.cognitree.gaurav.threadpools;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class Threads {

//    ConcurrentHashMap<String,Integer> wordCount = new ConcurrentHashMap<String,Integer>();
    List<Map<String, Integer>> threadWordCounts = new ArrayList<>();
    Map<String, Integer> totalWordCount = new HashMap<String, Integer>();

    List<String[]> totalLinesInFile = new ArrayList<>();


    public void readFile(){
        List<Map<String, Integer>> threadWordCounts = new ArrayList<>();
        Map<String, Integer> totalWordCount = new HashMap<String, Integer>();

        String fileLocation = "/Users/gc/IdeaProjects/WordCount/src/main/java/sampleFICT.txt";
        try (BufferedReader br = new BufferedReader(
                new FileReader(fileLocation))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split("\\r?\\n");
                List<String> wordList = new ArrayList<>();
                String[] words = lines[0].split(" ");
                for (int i = 0; i < words.length; i++) {
                    wordList.add(words[i]);
                }
                totalLinesInFile.add(words);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Total Lines in File: " + totalLinesInFile.size());
    }

    public void makeThreadPools() {
        readFile();
        int coreCount = Runtime.getRuntime().availableProcessors();
        System.out.println("Available Processors: " + coreCount);

        long startTime = System.nanoTime();

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ConcurrentHashMap<String,Integer> wordCount = new ConcurrentHashMap<String,Integer>();

        ThreadCountOneFile first = new ThreadCountOneFile(wordCount, totalLinesInFile, 0);
        ThreadCountOneFile second = new ThreadCountOneFile(wordCount, totalLinesInFile, 2000);
        ThreadCountOneFile third = new ThreadCountOneFile(wordCount, totalLinesInFile, 4000);

        executorService.execute(first);
        executorService.execute(second);
        executorService.execute(third);


        //each layer - prints, dump the map. few words. 1 lakh words,

//        executorService.shutdown();


        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
                //some ms it requires.
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

        System.out.println("first size" + first.wordCount.size());
        System.out.println("second size" +second.wordCount.size());
        System.out.println("third size" +third.wordCount.size());

        long elapsedTime = System.nanoTime() - startTime;
        System.out.println("Total execution time in millis: "
                + elapsedTime/1000000);


        threadWordCounts.add(first.wordCount);
        threadWordCounts.add(second.wordCount);
        threadWordCounts.add(third.wordCount);
        //i think the main thread starts going
//        System.out.println("final's word count" + wordCount);





        for (Map<String, Integer> wcount : threadWordCounts) {
            for (Map.Entry<String, Integer> entry : wcount.entrySet()) {
                String iword = entry.getKey();
                Integer icount = entry.getValue();
                Integer count = totalWordCount.get(iword);
                totalWordCount.put(iword, count == null ? icount : count + icount);
            }
        }

        int countValues = 0;
        for(Map.Entry<String, Integer> map: totalWordCount.entrySet()) {
            countValues += map.getValue();
        }
        System.out.println("total word count" + countValues);//1000000
    }


}
