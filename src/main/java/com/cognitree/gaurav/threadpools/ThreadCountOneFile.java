package com.cognitree.gaurav.threadpools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

class ThreadCountOneFile implements Runnable {
    List<String[]> totalLinesInFile = new ArrayList<>();
    int lineNumber;
    ConcurrentHashMap<String,Integer> wordCount;

    public ThreadCountOneFile(ConcurrentHashMap<String,Integer> wordCount, List<String[]> totalLinesInFile, int lineNumber) {
        this.totalLinesInFile = totalLinesInFile;
        this.lineNumber = lineNumber;
        this.wordCount = new ConcurrentHashMap<String,Integer>();
    }
    public synchronized Map<String, Integer> getWordCount() {
        return this.wordCount;
    }

    @Override
    public void run() {
        try {
            for (int line = lineNumber; line < lineNumber+2000; line++) {
                for (int word = 0; word < totalLinesInFile.get(line).length; word++) {
                    String currentWord = totalLinesInFile.get(line)[word];
                    if(wordCount.containsKey(currentWord)) {
                        wordCount.put(currentWord, wordCount.get(currentWord) + 1);
                    } else {
                        wordCount.put(currentWord, 1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
